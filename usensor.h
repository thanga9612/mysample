#ifndef usensor_h
#define usensor_h

#include <Arduino.h>

class usensor
{
  public:
    usensor(int pin1,int pin2);
    void calculate();
  private:
    int _pin1,_pin2;
};

#endif